<?php
session_start();
require_once "cnx.php";

if(!empty($_POST['email']) && !empty($_POST['password']) && isset($_POST['button']) )
{ 
    
    $email = htmlspecialchars($_POST['email']);
    $password = htmlspecialchars($_POST['password']);
    $email = strtolower($email);

    $check = $cnx->prepare('SELECT * from stagiaire where emailStagiaire= ?');
    $check->execute(array($email));
    $data = $check->fetch();
    $row = $check->rowCount();

    if($row>0){
        if(filter_var($email,FILTER_VALIDATE_EMAIL)){
            if($password === $data['passwordStagiaire']){
                $_SESSION['user'] = $data['idStagiaire'];
                header("location:profilStagiaire.php");
              }else{ header('location:loginstager.php?login_err=password'); die(); }

            }else{ header('location:loginstager.php?login_err=email');die();}
        }else{ header('location:loginstager.php?login_err=already');die();}
}


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OfpptNptes</title>
     <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="admin.css"> -->
       <link rel="stylesheet" href="logincss.css">

    <link href="assets/css/style.css" rel="stylesheet">
     <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
  
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <style>
  .ps {
    border: 1px solid;
	margin: 10px auto;
	padding: 10px 12px 12px 50px;
	background-repeat: no-repeat;
	background-position: 10px center;
	max-width: 460px;
  color: #D8000C;
	background-color: #FFBABA;
	background-image: url('https://i.imgur.com/GnyDvKN.png');
}
.register-photo .image-holder {
  display:table-cell;
  width:auto;
  background:url(img4.jpg);
  background-size:cover;
}
  
    </style>
  
</head>

<body>
    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center">
      <div class="container d-flex align-items-center justify-content-between">
        <div class="logo">
          <h1>
            <a href="index.html">Ofppt<a class="notes">Notes</a></a>
          </h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
          <ul>
            <li><a class="nav-link scrollto active" href="index.html">Home</a></li>
          
          </ul>
          <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
        <!-- .navbar -->
      </div>
    </header>
    <!-- End Header -->
    <div class="register-photo">
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post">
          
                <h2 class="text-center">  Interface de <strong>Stagaire</strong></h2>
                <?php
        if(isset($_GET['login_err']))
        {
            $err = htmlspecialchars($_GET['login_err']);
            switch($err){
                case 'password':
                ?>
                <div class="ps">
                   mot de passe incorrect
                </div>
                <?php 
                break;
                case 'email':
                    ?>
                    <div class="ps">
                        Erreur: email incorrect
                    </div>
                <?php
                break;
                case 'already';
                ?>
                <div class="ps">
                     Erreur: email incorrect

                  </div>
            <?php
            break;
                }
              }
              ?>
                <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Adresse e-mail "></div>
                <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Mot de passe"></div>
                <div class="form-group"><button class="btn btn-primary btn-block"name="button" type="submit">Se connecter</button></div>
                
            </form>
        </div>
    </div>
<!-- ======= Footer ======= -->
<footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>OfpptNotes</h3>
        <p>
          Adresse : <br>
          Hay Al Adarissa Route Ain Smen <br>
          B.P. 2590 30000 Fès <br><br>
          <strong>Phone:</strong> +212 5356-10333<br>
          <strong>Email:</strong> ofpptnote@gmail.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>Liens utiles</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Connecter-vous</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Contact</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>

        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Nos services</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Création de sites web</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Développement web</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Gestion des produits</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Commercialisation</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Conception graphique</a></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Rejoignez notre newsletter</h4>
        <p>amen quem nulla quae legam multos aute sint culpa legam noster magna</p>
        <form action="" method="post">
          <input type="email" name="email"><input type="submit" value="Subscribe">
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container d-lg-flex py-4">

  <div class="me-lg-auto text-center text-lg-start">
    <div class="copyright">
      &copy; Copyright <strong><span>OfpptNotes</span></strong>
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ -->
      Designed by <a href="https://bootstrapmade.com/">Youssef Lazar</a>
    </div>
  </div>
  <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
</body>

</html>