<?php

require_once "cnx.php";


// ===================================================================================
// ajouter Groupe

if(!empty($_POST['nomGroupe']) && isset($_POST['filier']) && isset($_POST["submitGroupe"])){
    try{
        $req = "INSERT INTO groupe(nomGroupe , idFilier) values (?,?)";
        $pre = $cnx->prepare($req);
        $pre->execute(array($_POST['nomGroupe'],$_POST['filier']));
        header("location:groupe.php?success=vous ajoute avec succéss");
    }catch(PDOException){
        header("location:groupe.php?err=ce groupe existe déja !!");
    }
}else{
    if(isset($_POST["submitGroupe"])){
        header("location:Groupe.php?err=remplire les champes !!");
    }
}

// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage Groupe
    $req3 = "select distinct * from Groupe g , filier f where g.idFilier = f.idFilier";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataGroupe = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select distinct * from Groupe g , filier f where g.idFilier = f.idFilier and nomGroupe like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataGroupe = $pre->fetchAll(PDO::FETCH_OBJ);
}

$req4 = "select idFilier , nomFilier from Filier";
$pre4 = $cnx->prepare($req4);
$pre4->execute();
$dataFilier = $pre4->fetchAll(PDO::FETCH_OBJ);




//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        /* fieldset{
            grid-template-columns: repeat(1,1fr);
        } */
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errG'])){?>
                    <p class="err"><?=$_GET['errG']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Ajouter Groupe</legend>
            <div>
                <input type="text" placeholder="Nom" name="nomGroupe">
            </div>
            <div>
                <select class="select" name="filier">
                    <?php
                    foreach($dataFilier as $dataFilier){
                        echo "<option value='$dataFilier->idFilier'> $dataFilier->nomFilier </option>";
                    }
                    ?>
                </select>
            </div>
            <div class="btn">
                <input type="submit" value="Ajouter" name="submitGroupe" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par nom" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID</th>
                <th>Nom Groupe</th>
                <th>Nom Filier</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataGroupe as $dataGroupe){ ?>
                    <tr>
                        <td><?= $dataGroupe->idGroupe ?></td>
                        <td><?= $dataGroupe->nomGroupe ?></td>
                        <td><?= $dataGroupe->nomFilier?></td>
                        <td>
                            <a href='delete.php?idG=<?= $dataGroupe->idGroupe ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierGroupe.php?id=<?= $dataGroupe->idGroupe ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>
</html>