<?php

require_once "cnx.php";

// ===================================================================================
// récupérer les valeurs pésedent
$id = $_GET['id'] ;
$reqet = "SELECT * from stagiaire where idStagiaire = ? ";
$prepar = $cnx->prepare($reqet);
$prepar->execute([$id]);
$oldData = $prepar->fetch(PDO::FETCH_OBJ);

// replissage de affecter groupe 
$req1 = "SELECT * from groupe";
$pre1 = $cnx->prepare($req1);
$pre1->execute();
$dataGroupe = $pre1->fetchAll(PDO::FETCH_OBJ);



// modifier Stagiaire
$err = "" ;
if(!empty($_POST["nomStagiaire"]) 
&& !empty($_POST["prenomStagiaire"]) 
&& !empty($_POST["adresseStagiaire"]) 
&& !empty($_POST["telStagiaire"]) 
&& !empty($_POST['g'])
&& !empty($_POST["emailStagiaire"]) 
&& !empty($_POST["passwordStagiaire"]) 
&& !empty($_POST["idGroupe"]) 
&& isset($_POST["submitStagiaire"]) ){
    $req = "update Stagiaire set 
    nomStagiaire = ? , prenomStagiaire = ? ,
     sexeStagiaire = ? , adresseStagiaire = ? , telStagiaire = ? , emailStagiaire = ? , passwordStagiaire = ? , idGroupe = ? where idStagiaire = ?";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomStagiaire"],
    $_POST["prenomStagiaire"],
    $_POST['g'],
    $_POST["adresseStagiaire"],
    $_POST["telStagiaire"],
    $_POST["emailStagiaire"],
    $_POST["passwordStagiaire"],
    $_POST["idGroupe"] ,
    $_GET["id"]
]);
header("location:stagiaire.php?success=vous Modifier avec succéss");
}else{
    if(isset($_POST["submitStagiaire"])){
        $err = "remplire les champes !!";
    }
}

if(isset($_POST['annuler'])){
    header("location:stagiaire.php");
}



//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
         <?php  }?>
        <fieldset>
            <legend>Modifier Stagiaire</legend>
            <div>
                <input type="text" placeholder="Nom" value="<?= $oldData->nomStagiaire ?>" name="nomStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Prenom" value="<?= $oldData->prenomStagiaire ?>" name="prenomStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Adresse" value="<?= $oldData->adresseStagiaire ?>" name="adresseStagiaire">
            </div>
            <div>
                <input type="tel" placeholder="Tel" value="<?= $oldData->telStagiaire ?>" name="telStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Email" value="<?= $oldData->emailStagiaire ?>" name="emailStagiaire">
            </div>
            <div>
                <input type="password" placeholder="Mot de passe" value="<?= $oldData->passwordStagiaire ?>" name="passwordStagiaire" />
            </div>
            <div>
                <select class="select" name="idGroupe" >
                    <?php 
                    foreach($dataGroupe as $dataGroupe){
                    ?>
                        <option <?php ($oldData->idGroupe == $dataGroupe->idGroupe) ? "selected" : ''?>  value="<?=$dataGroupe->idGroupe?>"  ><?=$dataGroupe->idGroupe . "-" .  $dataGroupe->nomGroupe?> </option>;
                    <?php }?>
                </select>
            </div>
            <div class="radios">
            <input value="Homme" type="radio" name="g" <?= $oldData->sexeStagiaire == 'Homme' ? 'checked' : ''?> >Homme 
            <input value="Femme" type="radio" name="g" <?= $oldData->sexeStagiaire == 'Femme' ? 'checked' : ''?>  >Femme 
            </div>

            <div class="btn">
                <input type="submit" value="Modifier" name="submitStagiaire">
                <input type="submit" value="Annuler" name="annuler">
            </div>

        </fieldset>

    </form>
</body>

</html>