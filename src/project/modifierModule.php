<?php

require_once "cnx.php";

// ===================================================================================
// récupérer les valeurs pésedent
$id = $_GET['id'] ;
$reqet = "SELECT * from Module where idModule = ? ";
$prepar = $cnx->prepare($reqet);
$prepar->execute([$id]);
$oldData = $prepar->fetch(PDO::FETCH_OBJ);




// modifier Module
$err = "" ;
if(!empty($_POST["nomModule"]) && isset($_POST["submitModule"])){
    $req = "update Module set 
    nomModule = ?  where idModule = ?";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomModule"],
    $_GET["id"]
]);
header("location:module.php?success=vous Modifier avec succéss");
}else{
    if(isset($_POST["submitModule"])){
        $err = "remplire les champes !!";
    }
}

if(isset($_POST['annuler'])){
    header("location:module.php");
}




//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        fieldset{
            grid-template-columns: repeat(1,1fr);
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
         <?php  }?>
        <fieldset>
            <legend>Modifier Module</legend>
            <div>
                <input type="text" placeholder="Nom" value="<?= $oldData->nomModule?>" name="nomModule">
            </div>
            <div class="btn">
                <input type="submit" value="Modifier" name="submitModule" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
    </form>
</body>
</html>