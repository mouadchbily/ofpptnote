<?php
session_start();
require("cnx.php") ;

require  'PHPMailer\src\Exception.php';
require  'PHPMailer\src\PHPMailer.php';
require  'PHPMailer\src\SMTP.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;



// Recupere les donnes de stagaire
if($_SESSION['user']){
  $sql1 = "select  * from prof where idProf = ?";
  $pre1 = $cnx->prepare($sql1);
  $pre1->execute([$_SESSION['user']]);
  $dataProf = $pre1->fetch(PDO::FETCH_OBJ);


  $sq = "select * from stagiaire s , groupe g , module m , notation n , enseignement e , filier f
  where s.idGroupe = g.idGroupe and s.idStagiaire = n.idStagiaire and n.idModule = m.idModule
  and g.idGroupe = e.idGroupe and g.idFilier = f.idFilier 
  and s.idStagiaire = ? and m.idModule = ? and g.idGroupe = ? and f.idFilier = ?";
  $pr = $cnx->prepare($sq);
  $pr->execute([$_GET['idS'] , $_GET['idM'] , $_GET['idG'] ,$_GET['idF']]);
  $infoStagiaire = $pr->fetch(PDO::FETCH_OBJ);



  $erreur = "" ;
if(isset($_GET['idS']) 
&& isset($_GET['idM']) 
&& isset($_GET['idG']) 
&& isset($_GET['idF']) 
&& !empty($_POST['note']) 
&& isset($_POST['submit'])
&& $_POST['note'] >=0 
&& $_POST['note'] <=20 ){

    $sql = "update notation n , stagiaire s  , groupe g set noteModule =?
    where n.idStagiaire = s.idStagiaire and s.idGroupe = g.idGroupe 
    and s.idStagiaire = ? and n.idModule = ? and s.idGroupe = ? and g.idFilier = ?";
    $pre = $cnx->prepare($sql);
    $pre->execute(array($_POST['note'] , $_GET['idS'] , $_GET['idM'] , $_GET['idG'] ,$_GET['idF'] ));
    $mail = new PHPMailer();
$mail->isSMTP();
$mail->Host = "smtp.gmail.com";
$mail->SMTPAuth = true;
$mail->Username="ofpptnote@gmail.com";
$mail->Password = "iwowsxwtapirpyam";
$mail->SMTPSecure ="tls";
$mail->Port = 587;
$mail->setFrom("ofpptnote@gmail.com");
$mail->Subject= "OFPPTNOTE";
$mail->Body = "Bonjour, vos points ont été ajoutés, vous pouvez les consulter maintenant";
$mail->addAddress($infoStagiaire->emailStagiaire);
$mail->send();
    header("location:profilProf.php");
}
else{
  if(isset($_POST['submit'])){
    $erreur = "la note doit etre entre 0 et 20 !!!";
  }

}

}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>ofpptNotes</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assetsprofile/img/favicon.png" rel="icon">
  <link href="assetsprofile/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assetsprofile/vendor/aos/aos.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assetsprofile/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assetsprofile/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v3.10.0
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>
    .err{
      background-color:#FF0B0B;
      color:#fff ;
      padding : 5px ;
      width : 60% ;
      border-radius:5px ;
    }
    #main {
      margin-left: 200px;
    }

    @media (max-width: 1199px) {
      #header {
        left: -400px;
      }

      #main {
        margin-left: 0;
      }

      .note{
        width: 100%;
        
      }
    }
  </style>
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <label for="img">
          <img src="<?= $dataProf->profile != null ?$dataProf->profile : "imgd"  ?>" alt="" class="img-fluid rounded-circle">
          <input type="file" id="img"/>
        </label>
        <h1 class="text-light"><a href="index.html"><?= ucfirst($dataProf->prenomProf)." ".ucfirst($dataProf->nomProf) ?></a></h1>
        <div class="social-links mt-3 text-center">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="profilProf.php" class="nav-link scrollto active"><span>Home</span></a></li>
          <li><a href="#about" class="nav-link scrollto active"> <span>Information Professeur</span></a></li>
          <li><a href="deconnexion.php?prof=true" class="nav-link scrollto active"><span>Déconnexion</span></a></li>
          
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  
  <main id="main" >
  <main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title">
          <h2>Information stagiaire</h2>
        </div>

        <div class="row">
          <div class="col-lg-4" data-aos="fade-right">
            <img src="<?= $infoStagiaire->profile != null ?$infoStagiaire->profile : "imgd"  ?>" class="img-fluid" alt="">
          </div>
          <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">      
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Nom :</strong> <span><?= ucfirst($infoStagiaire->nomStagiaire) ?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Prenom : </strong> <span><?= ucfirst($infoStagiaire->prenomStagiaire)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Tel : </strong> <span><?= ucfirst($infoStagiaire->telStagiaire)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Niveu: </strong> <span><?= ucfirst($infoStagiaire->niveu)?></span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Groupe :</strong> <span><?= ucfirst($infoStagiaire->nomGroupe)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Module : </strong> <span><?= ucfirst($infoStagiaire->nomModule)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Filier :</strong> <span><?= ucfirst($infoStagiaire->nomFilier)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Année scolaire : </strong> <span><?= ucfirst($infoStagiaire->anneeScolaire)?></span></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>

      </div>
    </section><!-- End About Section -->
   

  
      
    
  <form method="post">
    <div class="container select-block">
        <div class="row">
          
          <div class="parent-note ">
          <?php if($erreur){?>
          <p class="err"><?= $erreur  ?></p><?php }?>
          <div>
            <input class="col-4 note" type="text" name="note" placeholder="Ajouter note" />
            <input class="col-4 btn btn-primary note" type="submit" value="Ajouter note" name="submit" />
          </div>
        
          </div>
        
          </div>
          

          

         
          </div>


        
        </div>
    </div>


    </form>
  </main>


   

    

   

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>ofpptNotes</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/ -->
        Designed by <a id="linkfooter" href="https://bootstrapmade.com/">Yousssef lazar</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assetsprofile/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assetsprofile/vendor/aos/aos.js"></script>
  <script src="assetsprofile/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assetsprofile/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assetsprofile/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assetsprofile/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assetsprofile/vendor/typed.js/typed.min.js"></script>
  <script src="assetsprofile/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assetsprofile/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assetsprofile/js/main.js"></script>

</body>

</html>