<?php


session_start();
require_once("cnx.php");

if($_SESSION['user']){
  $sql1 = "select  * from admin where idAdmin = ?";
  $pre1 = $cnx->prepare($sql1);
  $pre1->execute([$_SESSION['user']]);
  $user = $pre1->fetch(PDO::FETCH_OBJ);


  if(isset($_POST['img'])){
    $req = "update admin set profile  = ? where idAdmin = ? " ;
    $pre = $cnx->prepare($req) ;
    $pre->execute([strtolower($_POST['img']),$_SESSION['user'] ]);
    echo "<script>
    document.location.href = './profilAdmin.php' ;
    </script>";
  }

}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>ofpptNotes</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assetsprofile/img/favicon.png" rel="icon">
  <link href="assetsprofile/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assetsprofile/vendor/aos/aos.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assetsprofile/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assetsprofile/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v3.10.0
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style>
    #hero {
      width: 100%;
      height: 100vh;
      background: url("<?= $user->profile != null ?$user->profile : ""  ?>") top center;
      background-repeat: no-repeat;
      background-size: cover;
    }
  </style>
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">
      <form method="post" id="form" enctype="multipart/form" >
        <label for="img" >
          <img  src="<?= $user->profile != null ?$user->profile : "imgd"  ?>" alt="" class="img-fluid rounded-circle">
          <input type="file" name="img"  id="img" accept=".jpg, .jpeg, .png"/>
        </label>
        </form>
        <h1 class="text-light"><a href="index.html">Admin</a></h1>
        <div class="social-links mt-3 text-center">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="groupe.php" class="nav-link scrollto active"> <span>Ajouter Groupes</span></a></li>
          <li><a href="prof.php" class="nav-link scrollto active"><span>Ajouter Professeurs</span></a></li>
          <li><a href="stagiaire.php" class="nav-link scrollto active"><span>Ajouter Stagiaires</span></a></li>
          <li><a href="filier.php" class="nav-link scrollto active"><span>Ajouter Filiers</span></a></li>
          <li><a href="module.php" class="nav-link scrollto active"><span>Ajouter Modules</span></a></li>
          <li><a href="affecterModule.php" class="nav-link scrollto active"><span>Affecter Modules</span></a></li>
          <li><a href="affectation.php" class="nav-link scrollto active"> <span>Affecter Professeurs</span></a></li>
          <li><a href="deconnexion.php?admin=true" class="nav-link scrollto active"><span>Déconnexion</span></a></li>
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">
      <h1>Admin</h1>
      <p>Bienvenue <span class="typed" data-typed-items="L'administrateur"></span></p>
    </div>
  </section><!-- End Hero -->



   

    

   

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>ofpptNotes</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/ -->
        Designed by <a id="linkfooter" href="https://bootstrapmade.com/">Yousssef lazar</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assetsprofile/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assetsprofile/vendor/aos/aos.js"></script>
  <script src="assetsprofile/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assetsprofile/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assetsprofile/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assetsprofile/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assetsprofile/vendor/typed.js/typed.min.js"></script>
  <script src="assetsprofile/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assetsprofile/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assetsprofile/js/main.js"></script>
  <script type="text/javascript">
    document.getElementById("img").onchange = function(){
      
      document.getElementById("form").submit();
      console.log("hellooooo")
    }
    
  </script>

</body>

</html>