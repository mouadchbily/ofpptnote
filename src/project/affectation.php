<?php

require_once "cnx.php";


// ===================================================================================
// affectation de prof
if(isset($_POST['submitenseignement']) 
&& !empty($_POST['prof'])  
&& !empty($_POST['grp'])
&& !empty($_POST['niveu'])
&& !empty($_POST['anneeScolaire'])){

    try{
        $req4 = "INSERT INTO enseignement values(?,?,?,?)";
        $pre4 = $cnx->prepare($req4);
        $pre4->execute(array($_POST['grp'] , $_POST['prof']  , $_POST['niveu'] , $_POST['anneeScolaire'] ));
        header("location:affectation.php?success=vous ajoute avec succéss");
    }catch(PDOException $e){
        header("location:affectation.php?err=ce affectation deja existe !!");
    }
}else{
    if(isset($_POST["submitenseignement"])){
        header("location:affectation.php?err=remplire les champes !!");
    }
}
// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage enseignement
    $req3 = "select distinct * from enseignement e , prof p  , groupe g where 
    e.idGroupe = g.idGroupe and e.idProf = p.idProf" ;
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataenseignement = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select distinct * from enseignement e , prof p  , groupe g where 
    e.idGroupe = g.idGroupe and e.idProf = p.idProf  and nomProf like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataenseignement = $pre->fetchAll(PDO::FETCH_OBJ);
}

// replissage de affecter groupe 
$req1 = "SELECT * from groupe";
$pre1 = $cnx->prepare($req1);
$pre1->execute();
$dataGroupe = $pre1->fetchAll(PDO::FETCH_OBJ);



// remplissage prof
$req3 = "select * from prof ";
$pre3 = $cnx->prepare($req3);
$pre3->execute();
$dataProf = $pre3->fetchAll(PDO::FETCH_OBJ);

// remplissage annee scolaire
$req5 = "select distinct anneeScolaire from enseignement ";
$pre5 = $cnx->prepare($req5);
$pre5->execute();
$dataScolaire = $pre5->fetchAll(PDO::FETCH_OBJ);

if(isset($_POST['annuler'])){
    header("location:profilAdmin.html");
}

//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errS'])){?>
                    <p class="err"><?=$_GET['errS']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Affectation de professeurs</legend>
            <div>
            Groupe :
            <select class="select" name="grp">
                <?php
                foreach($dataGroupe as $ad){
                    echo "<option value='$ad->idGroupe'>$ad->idGroupe - $ad->nomGroupe </option>";
                }
                ?>
            </select>
        </div>

        <div>
            professeur : 
            <select class="select" name="prof">
                <?php
                foreach($dataProf as $ap){
                    echo "<option value='$ap->idProf'>$ap->idProf - $ap->nomProf</option>";
                }
                ?>
            </select>
        </div>
        
        <div>
            Niveu : 
            <select class="select" name="niveu">
                <option value="1er annee">1er annee</option>
                <option value="2ème annee">2ème annee</option>
            </select>
        </div>
        <div>
            Année scolaire : 
            <select class="select" name="anneeScolaire">
                <?php
                foreach($dataScolaire as $dataScolaire){
                    echo "<option value='$dataScolaire->anneeScolaire'> $dataScolaire->anneeScolaire </option>";
                }
                ?>
            </select>
        </div>


        <div class="btn">
            <input type="submit" value="Ajouter" name="submitenseignement" >
            <input type="submit" value="Annuler" name="annuler">
        </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par id professeur" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID groupe</th>
                <th>Nom groupe</th>
                <th>ID professeur</th>
                <th>Nom professeur</th>
                <th>Niveu</th>
                <th>Année scolaire</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataenseignement as $dataenseignement){ ?>
                    <tr>
                        <td><?= $dataenseignement->idGroupe ?></td>
                        <td><?= $dataenseignement->nomGroupe ?></td>
                        <td><?= $dataenseignement->idProf ?></td>
                        <td><?= $dataenseignement->nomProf ?></td>
                        <td><?= $dataenseignement->niveu ?></td>
                        <td><?= $dataenseignement->anneeScolaire ?></td>
                        <td>
                            <a href='delete.php?idGr=<?=$dataenseignement->idGroupe?>&idPr=<?= $dataenseignement->idProf ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierAffectation.php?idG=<?=$dataenseignement->idGroupe?>&idP=<?= $dataenseignement->idProf ?>?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>
</html>