<?php

require_once "cnx.php";


// ===================================================================================



// affectation de prof
$err = "" ;
if(isset($_POST['submitenseignement']) 
&& !empty($_POST['prof'])  
&& !empty($_POST['grp'])
&& !empty($_POST['niveu'])
&& !empty($_POST['anneeScolaire'])
&& isset($_GET['idG'])
&& isset($_GET['idP'])){

    try{
        $req4 = "update enseignement set idGroupe = ? , idProf = ?  , niveu = ? , anneeScolaire = ? where 
        idGroupe = ? and idProf = ? " ;
        $pre4 = $cnx->prepare($req4);
        $pre4->execute(array($_POST['grp'] , $_POST['prof']  , $_POST['niveu'] , $_POST['anneeScolaire'],$_GET['idG'] ,$_GET['idP']));
        header("location:affectation.php?success=vous Modifier avec succéss");
    }catch(PDOException){
        $err = "ce affectation deja existe !!";
    }
    
}else{
    if(isset($_POST["submitenseignement"])){
        header("location:modifierAffectation.php?err=remplire les champes !!");
    }
}


// replissage de affecter groupe 
$req1 = "SELECT * from groupe";
$pre1 = $cnx->prepare($req1);
$pre1->execute();
$dataGroupe = $pre1->fetchAll(PDO::FETCH_OBJ);



// remplissage prof
$req3 = "select * from prof";
$pre3 = $cnx->prepare($req3);
$pre3->execute();
$dataProf = $pre3->fetchAll(PDO::FETCH_OBJ);


//=====================================================================================

// remplissage annee scolaire
$req5 = "select distinct anneeScolaire from enseignement ";
$pre5 = $cnx->prepare($req5);
$pre5->execute();
$dataScolaire = $pre5->fetchAll(PDO::FETCH_OBJ);


if(isset($_POST['annuler'])){
    header("location:affectation.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
        <?php  }?>
        <fieldset>
            <legend>Modifier affectation de professeurs</legend>
            <div>
            Groupe :
            <select class="select" name="grp">
                <?php
                foreach($dataGroupe as $ad){
                    echo "<option value='$ad->idGroupe'>$ad->idGroupe - $ad->nomGroupe </option>";
                }
                ?>
            </select>
        </div>

        <div>
            professeur : 
            <select class="select" name="prof">
                <?php
                foreach($dataProf as $ap){
                    echo "<option value='$ap->idProf'>$ap->idProf - $ap->nomProf</option>";
                }
                ?>
            </select>
        </div>
        <div>
            Niveu : 
            <select class="select" name="niveu">
                <option value="1er annee">1er annee</option>
                <option value="2ème annee">2ème annee</option>
            </select>
        </div>
        <div>
            Année scolaire : 
            <select class="select" name="anneeScolaire">
                <?php
                foreach($dataScolaire as $dataScolaire){
                    echo "<option value='$dataScolaire->anneeScolaire'> $dataScolaire->anneeScolaire </option>";
                }
                ?>
            </select>
        </div>


        <div class="btn">
            <input type="submit" value="Ajouter" name="submitenseignement" >
            <input type="submit" value="Annuler" name="annuler" >
        </div>
        </fieldset>
    </form>
</body>
</html>