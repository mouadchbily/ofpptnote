<style>
    *{
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    header{
        width: 100%;
        background-color: #fff;
        margin-bottom:50px ;
    }
    
    .nav{
        width: 100%;
        margin: auto;
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    h1{
        font-size: 26px;
        font-family:cursive;
        padding: 20px 30px;
    }
    h1 a:first-child{
        color: rgb(101, 172, 249);
        
    }
    h1 a:last-child{
        color:rgb(56, 205, 30) ;
    }
    .logo{
        width: 25%;

    }
    .links{
        width: 75%;
        display: flex;
        justify-content: space-around;
        font-weight: bold;
        
    }
    .links div{

        width:100%;

    }


    .links div a  {
        width: inherit;
        width: 100%;
        text-decoration: none;
        display: inline-block;
        font-size: 20px;
        text-align:center ;
        color:rgb(0, 157, 255);
        padding:27px 0 ;
        
    }
    .active{
        color:rgb(56, 205, 30)  !important;
    }
    .links div a:hover{
        color :rgb(56, 205, 30) ;
    }

    #parent-drop-down{
        position: relative;
    }

    ul{
        list-style-type:none;
    }
    .menu{
        background-color: #fff;
        position: absolute;
        top:80px;
        right : 0px ;
        border-radius :5px ;
    }
    .menu li a {
        width:210px ;
        padding: 20px 15px;
        padding-left:5px ;
        text-align:left ;
        border-bottom : solid 0.5px rgb(0, 157, 255) ;
        
    }
    #droodown{
        width:210px ;
        cursor: pointer;
        
    }
    
    
    .vu{
        display:none ;
    }
    

@media (max-width: 980px) {
    .nav{
        flex-direction :column;
        align-items:flex-start ;
        width:100%;
    }
    .links{
        flex-direction :column;
        width:100%;
    }
    .links div{
        border-bottom : 1px black solid ;
    }

    
    .links div a{
        text-align:left;
        width:100%;
        padding:10px 0 ;
        padding-left:10px;
    }
    #parent-drop-down li a{
        text-align:left ;
        
        width:100%;
    }

    .menu li  {  
        width:400px ;
    }

    .menu li  a{  
        padding-left:10px;
    }

    .menu{
        background-color: #fff;
        position: absolute;
        top:50px;
        right : 0px ;
        border-radius :5px ;
    }

  
}




</style>

<header>
    <div class="nav">
        <div class="logo">
            <h1><a>Ofppt</a><a>Notes</a></h1>
        </div>
        <div class="links">
            <div><a href="profilAdmin.php">Home</a></div>
            <div><a href="stagiaire.php">Stagiaires</a></div>
            <div><a href="prof.php">Professeurs</a></div>
            <div><a href="filier.php">Filiers</a></div>
            <div><a href="module.php">Modules</a></div>
            <div><a href="groupe.php">Groupes</a></div>
            <div id="parent-drop-down"><ul>
                <li><a id="droodown" >Affectation +</a>
                <ul class="menu vu">
                    <li><a class="affect" href="affectation.php">Affecter Professeurs</a></li>
                    <li><a class="affect" href="affecterModule.php">Affecter Modules</a></li>
                </ul>
                </li></ul>
            </div>
        </div>
    </div>
</header>
<script>
    var droodown = document.getElementById("droodown");
    droodown.addEventListener("click" , function(){
        document.getElementsByClassName("menu")[0].classList.toggle("vu");
        
    } ) 

    var allLink = document.querySelectorAll(".links a");
    var affect = document.querySelectorAll(".affect");

    for(let i = 0 ; i < allLink.length ; i++){
        if(location.href.includes(allLink[i].href)){
            allLink[i].classList.add("active")
        }
    }

    for(let i = 0 ; i < affect.length ; i++){
        if(location.href.includes(affect[i].href)){
            droodown.classList.add("active")
        }
    }


   
   

</script>