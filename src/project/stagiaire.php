<?php

require_once "cnx.php";


// ===================================================================================
// ajouter stagiaire
if(!empty($_POST["nomStagiaire"]) 
&& !empty($_POST["prenomStagiaire"]) 
&& !empty($_POST["adresseStagiaire"]) 
&& !empty($_POST["telStagiaire"]) 
&& !empty($_POST['g'])
&& !empty($_POST["emailStagiaire"]) 
&& !empty($_POST["passwordStagiaire"]) 
&& isset($_POST["submitStagiaire"]) 
&& !empty($_POST["idGroupe"]) 
){
    $req = "INSERT INTO stagiaire(nomStagiaire,prenomStagiaire,sexeStagiaire,adresseStagiaire,telStagiaire,emailStagiaire,passwordStagiaire,idGroupe) values(?,?,?,?,?,?,?,?)";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomStagiaire"],
    $_POST["prenomStagiaire"],
    $_POST['g'],
    $_POST["adresseStagiaire"],
    $_POST["telStagiaire"],
    $_POST["emailStagiaire"],
    $_POST["passwordStagiaire"],
    $_POST["idGroupe"]
]);
header("location:stagiaire.php?success=vous ajoute avec succéss");
}else{
    if(isset($_POST["submitStagiaire"])){
        header("location:stagiaire.php?err=remplire les champes !!");
    }
}

// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage stagiaire
    $req3 = "select * from stagiaire s , groupe g where s.idGroupe = g.idGroupe";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataStagiaire = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select * from stagiaire s , groupe g where s.idGroupe = g.idGroupe and nomStagiaire like ? ';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataStagiaire = $pre->fetchAll(PDO::FETCH_OBJ);
}

// replissage de affecter groupe 
$req1 = "SELECT * from groupe";
$pre1 = $cnx->prepare($req1);
$pre1->execute();
$dataGroupe = $pre1->fetchAll(PDO::FETCH_OBJ);


//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>

    <style>
        .radios{
            color :white ;
            grid-column:2/3;
            width: 90%;
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errS'])){?>
                    <p class="err"><?=$_GET['errS']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Ajouter Stagiaire</legend>
            <div>
                <input type="text" placeholder="Nom" name="nomStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Prenom" name="prenomStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Adresse" name="adresseStagiaire">
            </div>
            <div>
                <input type="tel" placeholder="Tel" name="telStagiaire">
            </div>
            <div>
                <input type="text" placeholder="Email" name="emailStagiaire">
            </div>
            <div>
                <input type="password" placeholder="Mot de passe" name="passwordStagiaire" />
            </div>
            <div>
                <select class="select" name="idGroupe">
                    <?php 
                    foreach($dataGroupe as $dataGroupe){
                        echo "<option value='$dataGroupe->idGroupe'>$dataGroupe->idGroupe - $dataGroupe->nomGroupe </option>";
                    }
                    ?>
                </select>
            </div>
            <div class="radios">
                <input type="radio" name="g" value="Homme">Homme <input type="radio" name="g" value="Femme">Femme 
            </div>

            <div class="btn">
                <input type="submit" value="Ajouter" name="submitStagiaire" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par nom" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Sexe</th>
                <th>Adresse</th>
                <th>Tel</th>
                <th>Email</th>
                <th>mot de passe</th>
                <th>Groupe</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataStagiaire as $dataStagiaire){ ?>
                    <tr>
                        <td><?= $dataStagiaire->idStagiaire ?></td>
                        <td><?= $dataStagiaire->nomStagiaire ?></td>
                        <td><?= $dataStagiaire->prenomStagiaire ?></td>
                        <td><?= $dataStagiaire->sexeStagiaire ?></td>
                        <td><?= $dataStagiaire->adresseStagiaire ?></td>
                        <td><?= $dataStagiaire->telStagiaire ?></td>
                        <td><?= $dataStagiaire->emailStagiaire ?></td>
                        <td><?= $dataStagiaire->passwordStagiaire ?></td>
                        <td><?= $dataStagiaire->nomGroupe?></td>
                        <td>
                            <a href='delete.php?idS=<?= $dataStagiaire->idStagiaire ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierStagiaire.php?id=<?= $dataStagiaire->idStagiaire ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>

</html>