
<?php

require_once "cnx.php" ;

if(isset($_GET['idG'])){ 

    try{
        $del = "DELETE FROM groupe where idGroupe = ? "; 
        $pre = $cnx->prepare($del) ; 
        $pre->execute(array($_GET['idG'])) ; 
        header("location:groupe.php");
    }catch(PDOException){
        header("location:groupe.php?errG=ne peut pas supprimer ce Groupe !!!");
    }
    
}

if(isset($_GET['idF'])){

    try{
        $del = "DELETE FROM filier where idFilier = ? "; 
        $pre = $cnx->prepare($del) ; 
        $pre->execute(array($_GET['idF'])) ; 
        header("location:filier.php");
    }catch(PDOException){
        header("location:filier.php?errF=ne peut pas supprimer ce Filier !!!");
    }
    
}

if(isset($_GET['idM'])){

    try{
        $del = "DELETE FROM module where idModule = ? "; 
        $pre = $cnx->prepare($del) ; 
        $pre->execute(array($_GET['idM'])) ; 
        header("location:module.php");
    }catch(PDOException){
        header("location:module.php?errM=ne peut pas supprimer ce Module !!!");
    }
    
}

if(isset($_GET['idP'])){

    try{
        $del = "DELETE FROM prof where idProf = ? "; 
        $pre = $cnx->prepare($del) ; 
        $pre->execute(array($_GET['idP'])) ; 
        header("location:prof.php");
    }catch(PDOException){
        header("location:prof.php?errP=ne peut pas supprimer ce professeur !!!");
    }
    
}

// delete stagiaire
if(isset($_GET['idS'])){
    try{
        $del = "DELETE FROM stagiaire where idStagiaire = ? "; 
        $pre = $cnx->prepare($del) ; 
        $pre->execute(array($_GET['idS'])) ; 
        header("location:stagiaire.php");
    }catch(PDOException){
        header("location:stagiaire.php?errS=ne peut pas supprimer ce stagiaire !!!");
    }
    
}
// delete affectation prof
if(isset($_GET['idGr']) && isset($_GET['idPr'])){
    $del = "DELETE FROM enseignement where idGroupe = ? and idProf = ? "; 
    $pre = $cnx->prepare($del) ; 
    $pre->execute(array($_GET['idGr'],$_GET['idPr'])) ; 
    header("location:affectation.php");
}

// delete affectation Module
if(isset($_GET['idFi']) && isset($_GET['idMo'])){
    $del = "DELETE FROM programme where idFilier = ? and idModule = ?  "; 
    $pre = $cnx->prepare($del) ; 
    $pre->execute(array($_GET['idFi'],$_GET['idMo'])) ; 
    header("location:affecterModule.php");
}

    
    





