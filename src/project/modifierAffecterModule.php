<?php

require_once "cnx.php";


// ===================================================================================



// affectation de prof
$err = "" ;
if(isset($_POST['submitProgramme']) 
&& !empty($_POST['filier']) 
&& !empty($_POST['module'])  
&& !empty($_POST['coeff'])
&& !empty($_POST['hours'])
&& isset($_GET['idFi'])
&& isset($_GET['idMo'])
){

    try{
        $req4 = "update programme set idFilier = ? , idModule = ? , coeff = ? , nbrHours = ? where 
        idFilier = ? and idModule = ? " ;
        $pre4 = $cnx->prepare($req4);
        $pre4->execute(array($_POST['filier'] , $_POST['module'] , $_POST['coeff'] , $_POST['hours'],$_GET['idFi'] ,$_GET['idMo']));
        header("location:affecterModule.php?success=vous Modifier avec succéss");
    }catch(PDOException){
        $err = "ce affectation deja existe !!";
    }
    
}else{
    if(isset($_POST["submitProgramme"])){
        header("location:modifierAffecterModule.php?err=remplire les champes !!");
    }
}

// remplisage filier
$req2 = "select idFilier , nomFilier from filier";
$pre2 = $cnx->prepare($req2);
$pre2->execute();
$dataFilier = $pre2->fetchAll(PDO::FETCH_OBJ);

// remplissage module
$req3 = "select * from module ";
$pre3 = $cnx->prepare($req3);
$pre3->execute();
$dataModule = $pre3->fetchAll(PDO::FETCH_OBJ);

if(isset($_POST['annuler'])){
    header("location:affecterModule.php");
}


//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
        <?php  }?>
        <fieldset>
            <legend>Modifier Affectation des Modules</legend>
            <div>
            Filier :
            <select class="select" name="filier">
                <?php
                foreach($dataFilier as $ad){
                    echo "<option value='$ad->idFilier'>$ad->idFilier - $ad->nomFilier </option>";
                }
                ?>
            </select>
        </div>

        <div>
            Module : 
            <select class="select" name="module">
                <?php
                foreach($dataModule as $ap){
                    echo "<option value='$ap->idModule'>$ap->idModule - $ap->nomModule</option>";
                }
                ?>
            </select>
        </div>
        <div>
            Coefficients : 
            <select class="select" name="coeff">
            <?php for($i = 1 ; $i <= 10 ; $i++){ ?>

            <option value="<?=$i?>" ><?=$i?></option>

            <?php } ?>
            </select>
        </div>
        <div>
            nombre heurs : 
            <select class="select" name="hours">
                <?php for($i = 5 ; $i <= 150 ; $i += 5){ ?>

                    <option value="<?=$i."H"?>" ><?=$i."H"?></option>
                    
                <?php } ?>
            </select>
        </div>


        <div class="btn">
            <input type="submit" value="Modifier" name="submitProgramme" >
            <input type="submit" value="Annuler" name="annuler">
        </div>
        </fieldset>
    </form>
</body>
</html>