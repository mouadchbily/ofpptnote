<?php

require_once "cnx.php";

// ===================================================================================
// récupérer les valeurs pésedent
$id = $_GET['id'] ;
$reqet = "SELECT * from filier where idFilier = ? ";
$prepar = $cnx->prepare($reqet);
$prepar->execute([$id]);
$oldData = $prepar->fetch(PDO::FETCH_OBJ);




// modifier Stagiaire
$err = "" ;
if(!empty($_POST["nomFilier"]) && isset($_POST["submitFilier"])){
    $req = "update filier set 
    nomFilier = ?  where idFilier = ?";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomFilier"],
    $_GET["id"]
]);
header("location:filier.php?success=vous Modifier avec succéss");
}else{
    if(isset($_POST["submitFilier"])){
        $err = "remplire les champes !!";
    }
}

if(isset($_POST['annuler'])){
    header("location:filier.php");
}




//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        fieldset{
            grid-template-columns: repeat(1,1fr);
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
        <?php  }?>
        <fieldset>
            <legend>Modifier Filier</legend>
            <div>
                <input type="text" placeholder="Nom" value="<?= $oldData->nomFilier?>" name="nomFilier">
            </div>
            <div class="btn">
                <input type="submit" value="Modifier" name="submitFilier" >
                <input type="reset" value="Annuler">
            </div>
        </fieldset>
    </form>
</body>
</html>