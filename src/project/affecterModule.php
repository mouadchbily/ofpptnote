<?php

require_once "cnx.php";


// ===================================================================================
// affectation de Module
if(isset($_POST['submitProgramme']) 

&& !empty($_POST['filier'])  
&& !empty($_POST['module'])
&& !empty($_POST['coeff'])
&& !empty($_POST['hours'])){

    try{
        $req4 = "INSERT INTO programme values(?,?,?,?)";
        $pre4 = $cnx->prepare($req4);
        $pre4->execute(array($_POST['filier'] , $_POST['module'] , $_POST['coeff'] , $_POST['hours']));
        header("location:affecterModule.php?success=vous ajoute avec succéss");
    }catch(PDOException $e){
        header("location:affecterModule.php?err=ce affectation deja existe !!");
    }
}else{
    if(isset($_POST["submitProgramme"])){
        header("location:affecterModule.php.php?err=remplire les champes !!");
    }
}
// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage programme
    $req3 = "select distinct * from programme p , filier f , module m  where 
    p.idFilier = f.idFilier and p.idModule = m.idModule";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataProgramme = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select distinct * from programme p , filier f , module m  where 
    p.idFilier = f.idFilier and p.idModule = m.idModule and nomFilier like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataProgramme = $pre->fetchAll(PDO::FETCH_OBJ);
}



// remplisage filier
$req2 = "select idFilier , nomFilier from filier";
$pre2 = $cnx->prepare($req2);
$pre2->execute();
$dataFilier = $pre2->fetchAll(PDO::FETCH_OBJ);

// remplissage module
$req3 = "select * from module ";
$pre3 = $cnx->prepare($req3);
$pre3->execute();
$dataModule = $pre3->fetchAll(PDO::FETCH_OBJ);


if(isset($_POST['annuler'])){
    header("location:profilAdmin.html");
}

//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errS'])){?>
                    <p class="err"><?=$_GET['errS']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Affectation des Modules</legend>
            <div>
            Filier :
            <select class="select" name="filier">
                <?php
                foreach($dataFilier as $ad){
                    echo "<option value='$ad->idFilier'>$ad->idFilier - $ad->nomFilier </option>";
                }
                ?>
            </select>
        </div>

        <div>
            Module : 
            <select class="select" name="module">
                <?php
                foreach($dataModule as $ap){
                    echo "<option value='$ap->idModule'>$ap->idModule - $ap->nomModule</option>";
                }
                ?>
            </select>
        </div>
        <div>
            Coefficients : 
            <select class="select" name="coeff">
            <?php for($i = 1 ; $i <= 10 ; $i++){ ?>

            <option value="<?=$i?>" ><?=$i?></option>

            <?php } ?>
            </select>
        </div>
        <div>
            nombre heurs : 
            <select class="select" name="hours">
                <?php for($i = 5 ; $i <= 150 ; $i += 5){ ?>

                    <option value="<?=$i."H"?>" ><?=$i."H"?></option>
                    
                <?php } ?>
            </select>
        </div>


        <div class="btn">
            <input type="submit" value="Ajouter" name="submitProgramme" >
            <input type="submit" value="Annuler" name="annuler">
        </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par id professeur" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID Filier</th>
                <th>Nom Filier</th>
                <th>ID Module</th>
                <th>Nom Module</th>
                <th>Coeffcients</th>
                <th>nombre heurs</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataProgramme as $dataProgramme){ ?>
                    <tr>
                        <td><?= $dataProgramme->idFilier ?></td>
                        <td><?= $dataProgramme->nomFilier ?></td>
                        <td><?= $dataProgramme->idModule ?></td>
                        <td><?= $dataProgramme->nomModule ?></td>
                        <td><?= $dataProgramme->coeff ?></td>
                        <td><?= $dataProgramme->nbrHours ?></td>
                        <td>
                            <a href='delete.php?idFi=<?=$dataProgramme->idFilier?>&idMo=<?= $dataProgramme->idModule ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierAffecterModule.php?idFi=<?=$dataProgramme->idFilier?>&idMo=<?= $dataProgramme->idModule ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>
</html>