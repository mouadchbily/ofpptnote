<?php

require_once "cnx.php";

// ===================================================================================
// récupérer les valeurs pésedent
$id = $_GET['id'] ;
$reqet = "SELECT * from prof where idProf = ? ";
$prepar = $cnx->prepare($reqet);
$prepar->execute([$id]);
$oldData = $prepar->fetch(PDO::FETCH_OBJ);





// modifier professeurs
$err = "" ;
if(!empty($_POST["nomProf"]) 
&& !empty($_POST["prenomProf"]) 
&& !empty($_POST["adresseProf"]) 
&& !empty($_POST["telProf"]) 
&& !empty($_POST['g'])
&& !empty($_POST["emailProf"]) 
&& !empty($_POST["passwordProf"]) 
&& isset($_POST["submitProf"]) ){
    $req = "update prof set 
    nomProf = ? , prenomProf = ? ,
     sexeProf = ? , adresseProf = ? , telProf = ? , emailProf = ? , password = ? where idProf = ?";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomProf"],
    $_POST["prenomProf"],
    $_POST['g'],
    $_POST["adresseProf"],
    $_POST["telProf"],
    $_POST["emailProf"],
    $_POST["passwordProf"],
    $_GET["id"]
]);
header("location:prof.php?success=vous Modifier avec succéss");
}else{
    if(isset($_POST["submitProf"])){
        $err = "remplire les champes !!";
    }
}

if(isset($_POST['annuler'])){
    header("location:prof.php");
}


//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
         <?php  }?>
        <fieldset>
            <legend>Modifier Professeur</legend>
            <div>
                <input type="text" placeholder="Nom" value="<?= $oldData->nomProf ?>" name="nomProf">
            </div>
            <div>
                <input type="text" placeholder="Prenom" value="<?= $oldData->prenomProf ?>" name="prenomProf">
            </div>
            <div>
                <input type="text" placeholder="Adresse" value="<?= $oldData->adresseProf ?>" name="adresseProf">
            </div>
            <div>
                <input type="tel" placeholder="Tel" value="<?= $oldData->telProf ?>" name="telProf">
            </div>
            <div>
                <input type="text" placeholder="Email" value="<?= $oldData->emailProf ?>" name="emailProf">
            </div>
            <div>
                <input type="password" placeholder="Mot de passe" value="<?= $oldData->password ?>" name="passwordProf" />
            </div>
            <div class="radios">
                <input value="Homme" type="radio" name="g" <?= $oldData->sexeProf == 'Homme' ? 'checked' : ''?> >Homme <input <?= $oldData->sexeProf == 'Femme' ? 'checked' : ''?> type="radio" name="g" value="Femme">Femme 
            </div>

            <div class="btn">
                <input type="submit" value="Modifier" name="submitProf">
                <input type="submit" value="Annuler" name="annuler">
            </div>
        </fieldset>
    </form>
</body>

</html>