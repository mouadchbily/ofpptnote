<?php

require_once "cnx.php";


// ===================================================================================
// ajouter filier

if(!empty($_POST['nomFilier']) && isset($_POST["submitFilier"])){
    $req = "INSERT INTO filier(nomFilier) values (?)";
    $pre = $cnx->prepare($req);
    $pre->execute(array($_POST['nomFilier']));
    header("location:filier.php?success=vous ajoute avec succéss");
}else{
    if(isset($_POST["submitFilier"])){
        header("location:filier.php?err=remplire les champes !!");
    }
}

// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage Filier
    $req3 = "select * from filier";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataFilier = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select * from filier where nomFilier like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataFilier = $pre->fetchAll(PDO::FETCH_OBJ);
}




//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        fieldset{
            grid-template-columns: repeat(1,1fr);
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errF'])){?>
                    <p class="err"><?=$_GET['errF']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Ajouter Filier</legend>
            <div>
                <input type="text" placeholder="Nom" name="nomFilier">
            </div>
            <div class="btn">
                <input type="submit" value="Ajouter" name="submitFilier" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par nom" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataFilier as $dataFilier){ ?>
                    <tr>
                        <td><?= $dataFilier->idFilier ?></td>
                        <td><?= $dataFilier->nomFilier ?></td>
                        <td>
                            <a href='delete.php?idF=<?= $dataFilier->idFilier ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierFilier.php?id=<?= $dataFilier->idFilier ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>

</html>