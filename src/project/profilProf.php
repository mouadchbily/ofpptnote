<?php

session_start();
require("cnx.php");

if($_SESSION['user']){
  $sql1 = "select  * from prof where idProf = ?";
  $pre1 = $cnx->prepare($sql1);
  $pre1->execute([$_SESSION['user']]);
  $dataProf = $pre1->fetch(PDO::FETCH_OBJ);


  $sql2 = "SELECT DISTINCT anneeScolaire from enseignement ";
  $pre2 = $cnx->prepare($sql2);
  $pre2->execute();
  $infoAnnee = $pre2->fetchAll(PDO::FETCH_OBJ);


  $sql2 = "SELECT DISTINCT nomFilier , f.idFilier  from prof p , enseignement e , groupe g , filier f WHERE p.idProf = e.idProf 
  and e.idGroupe = g.idGroupe and g.idFilier = f.idFilier and p.idProf = ?";
  $pre2 = $cnx->prepare($sql2);
  $pre2->execute([$_SESSION['user']]);
  $infoFilier = $pre2->fetchAll(PDO::FETCH_OBJ);

  $sql3 = "SELECT distinct nomGroupe , g.idGroupe  from prof p , enseignement e , groupe g  WHERE p.idProf = e.idProf 
  and e.idGroupe = g.idGroupe and p.idProf = ?";
  $pre3 = $cnx->prepare($sql3);
  $pre3->execute([$_SESSION['user']]);
  $infoGroupe = $pre3->fetchAll(PDO::FETCH_OBJ);


  $sql4 = "SELECT DISTINCT m.idModule , nomModule  from prof p ,module m , notation n WHERE p.idProf = n.idProf 
  and n.idModule = m.idModule and p.idProf = ?";
  $pre4 = $cnx->prepare($sql4);
  $pre4->execute([$_SESSION['user']]);
  $infoModule = $pre4->fetchAll(PDO::FETCH_OBJ);


  if(isset($_POST['submit']) && isset($_POST['filier'])  && isset($_POST['groupe']) && isset($_POST['module']) && isset($_POST['annee'])){

    $sq = "select DISTINCT f.idFilier , g.idGroupe , s.idStagiaire ,m.idModule , nomStagiaire , nomGroupe , nomModule , noteModule , nomFilier , prenomStagiaire
    from stagiaire s , groupe g , module m , notation n , enseignement e , filier f
    where s.idGroupe = g.idGroupe and s.idStagiaire = n.idStagiaire and n.idModule = m.idModule
    and g.idGroupe = e.idGroupe and g.idFilier = f.idFilier 
    and e.anneeScolaire = ? and m.idModule = ? and g.idGroupe = ? and f.idFilier =?";
    $pr = $cnx->prepare($sq);
    $pr->execute([$_POST['annee'],$_POST['module'],$_POST['groupe'] , $_POST['filier']]);
    $infoStagiaire = $pr->fetchAll(PDO::FETCH_ASSOC);

  }


 
  if(isset($_POST['img'])){
    $req = "update prof set profile = ? where idProf = ? " ;
    $pre = $cnx->prepare($req) ;
    $pre->execute([strtolower($_POST['img']),$_SESSION['user'] ]);
    echo "<script>
    document.location.href = './profilProf.php' ;
    </script>";

  }



}







?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>ofpptNotes</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assetsprofile/img/favicon.png" rel="icon">
  <link href="assetsprofile/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assetsprofile/vendor/aos/aos.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assetsprofile/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assetsprofile/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assetsprofile/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v3.10.0
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <style>
    #hero {
      width: 100%;
      height: 100vh;
      background: url("<?= $dataProf->profile != null ?$dataProf->profile : ""  ?>") top center;
      background-repeat: no-repeat;
      background-size: cover;
    }
  </style>
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <i class="bi bi-list mobile-nav-toggle d-xl-none"></i>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">
      <div class="profile">
        <form method="post" id="form" enctype="multipart/form" >
        <label for="img" >
          <img  src="<?= $dataProf->profile != null ?$dataProf->profile : "imgd"  ?>" alt="" class="img-fluid rounded-circle">
          <input type="file" name="img"  id="img" accept=".jpg, .jpeg, .png"/>
        </label>
        </form>
        <h1 class="text-light"><a href="index.html"><?= ucfirst($dataProf->prenomProf)." ".ucfirst($dataProf->nomProf) ?></a></h1>
        <div class="social-links mt-3 text-center">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>

      <nav id="navbar" class="nav-menu navbar">
        <ul>
          <li><a href="#hero" class="nav-link scrollto active"><span>Home</span></a></li>
          <li><a href="#about" class="nav-link scrollto active"><span>Information Professeur</span></a></li>
          <li><a href="#notes" class="nav-link scrollto active"><span>Ajouter Notes</span></a></li>
          <li><a href="deconnexion.php?prof=true" class="nav-link scrollto active"><span>Déconnexion</span></a></li>
        </ul>
      </nav><!-- .nav-menu -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">
      <h1><?= ucfirst($dataProf->prenomProf)." ".ucfirst($dataProf->nomProf) ?></h1>
      <p>Bienvenue <span class="typed" data-typed-items="<?= ucfirst($dataProf->prenomProf)." ".ucfirst($dataProf->nomProf) ?>"></span></p>
    </div>
  </section><!-- End Hero -->
  <main id="main">
    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title" id="about">
          <h2>Information Professeur</h2>
        </div>

        <div class="row">
          <div class="col-lg-4" data-aos="fade-right">
            <img src="<?= $dataProf->profile != null ?$dataProf->profile : "imgd"  ?>" class="img-fluid" alt="">
          </div>
          <div class="col-lg-8 pt-4 pt-lg-0 content" data-aos="fade-left">      
            <div class="row">
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Nom :</strong> <span><?= ucfirst($dataProf->nomProf) ?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Prenom : </strong> <span><?= ucfirst($dataProf->prenomProf)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Tel : </strong> <span><?= ucfirst($dataProf->telProf)?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>ville : </strong> <span>Fes</span></li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul>
                  <li><i class="bi bi-chevron-right"></i> <strong>Age :</strong> <span>22</span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Année Scolaire : </strong> <span><?= 2023 ?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>Email :</strong> <span><?= ucfirst($dataProf->emailProf) ?></span></li>
                  <li><i class="bi bi-chevron-right"></i> <strong>niveux scolaire : </strong> <span>2 Année</span></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

  
      
    
    <form method="post" >
    <div class="container select-block ">
        <div class="row ">
          <select class="col-4 select" name="annee">
            <?php foreach($infoAnnee as $info){?>
              <option value="<?= $info->anneeScolaire ?>"> <?= $info->anneeScolaire ?> </option>
            <?php }?>
          </select>
          <select class="col-4 select" name="filier">
            <?php foreach($infoFilier as $info){?>
              <option value="<?= $info->idFilier ?>"> <?= $info->nomFilier?> </option>
            <?php }?>
          </select>
          <select class="col-4 select" name="groupe">
            <?php foreach($infoGroupe as $info){?>
              <option value="<?= $info->idGroupe ?>"> <?= $info->nomGroupe?> </option>
            <?php } ?>
          </select>
          <select class="col-4 select " name="module">
            <?php foreach($infoModule as $info){?>
              <option value="<?= $info->idModule ?>"> <?= $info->nomModule?> </option>
            <?php } ?>
          </select>

          <input id="notes" class="col-9 btn btn-primary select exept" type="submit" value="Rechercher" name="submit"/>

          <div class="over-flow">
          <?php if(isset($_POST['submit']) ){?>
          <table  class="table exept bg-dark text-light " >
            <tr>
                <th>Nom & Prenom</th>
                <th>Filier</th>
                <th>Module</th>
                <th>Groupe</th>
                <th>Notes</th>
                <th>Ajouter</th>
                <th>Modifier</th>
            </tr>
            <?php 
            foreach($infoStagiaire as $infoStagiaire){?>
            <tr>
                <td><?= $infoStagiaire['nomStagiaire'] ." ".$infoStagiaire['prenomStagiaire']?></td>
                <td><?= $infoStagiaire['nomFilier']?></td>
                <td><?= $infoStagiaire['nomModule']?></td>
                <td><?= $infoStagiaire['nomGroupe']?></td>
                <td><?= $infoStagiaire['noteModule']?></td>
                <td><a href="ajouterNote.php?idS=<?= $infoStagiaire['idStagiaire'] ?>&idM=<?= $infoStagiaire['idModule'] ?>&idG=<?= $infoStagiaire['idGroupe'] ?>&idF=<?= $infoStagiaire['idFilier'] ?>" class="btn btn-primary" >Ajouter</a></td>
                <td><a href="ajouterNote.php?idS=<?= $infoStagiaire['idStagiaire'] ?>&idM=<?= $infoStagiaire['idModule'] ?>&idG=<?= $infoStagiaire['idGroupe'] ?>&idF=<?= $infoStagiaire['idFilier'] ?>" class="btn btn-success" >Modifier</a></td>
            </tr>
            <?php }?>
        </table>
        <?php }?>
          </div>


        
        </div>
    </div>


    </form>


   

    

   

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>ofpptNotes</span></strong>
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/ -->
        Designed by <a id="linkfooter" href="https://bootstrapmade.com/">Yousssef lazar</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assetsprofile/vendor/purecounter/purecounter_vanilla.js"></script>
  <script src="assetsprofile/vendor/aos/aos.js"></script>
  <script src="assetsprofile/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assetsprofile/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assetsprofile/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assetsprofile/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assetsprofile/vendor/typed.js/typed.min.js"></script>
  <script src="assetsprofile/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assetsprofile/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assetsprofile/js/main.js"></script>

  <script type="text/javascript">
    document.getElementById("img").onchange = function(){
      
      document.getElementById("form").submit();
      console.log("hellooooo")
    }
    
  </script>
  
</body>
</html>