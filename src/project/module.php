<?php

require_once "cnx.php";


// ===================================================================================
// ajouter Module

if(!empty($_POST['nomModule']) && isset($_POST["submitModule"])){
    $req = "INSERT INTO module(nomModule) values (?)";
    $pre = $cnx->prepare($req);
    $pre->execute(array($_POST['nomModule']));
    header("location:module.php?success=vous ajoute avec succéss");
}else{
    if(isset($_POST["submitModule"])){
        header("location:module.php?err=remplire les champes !!");
    }
}

// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage Module
    $req3 = "select * from module";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataModule = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select * from module where nomModule like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataModule = $pre->fetchAll(PDO::FETCH_OBJ);
}


//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        fieldset{
            grid-template-columns: repeat(1,1fr);
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errM'])){?>
                    <p class="err"><?=$_GET['errM']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Ajouter Module</legend>
            <div>
                <input type="text" placeholder="Nom" name="nomModule">
            </div>
            <div class="btn">
                <input type="submit" value="Ajouter" name="submitModule" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par nom" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataModule as $dataModule){ ?>
                    <tr>
                        <td><?= $dataModule->idModule ?></td>
                        <td><?= $dataModule->nomModule ?></td>
                        <td>
                            <a href='delete.php?idM=<?= $dataModule->idModule ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierModule.php?id=<?= $dataModule->idModule ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>

</html>