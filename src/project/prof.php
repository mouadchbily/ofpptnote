<?php

require_once "cnx.php";

// ===================================================================================
// ajouter prof
if(!empty($_POST["nomProf"]) 
&& !empty($_POST["prenomProf"]) 
&& !empty($_POST["adresseProf"]) 
&& !empty($_POST["telProf"]) 
&& !empty($_POST['g'])
&& !empty($_POST["emailProf"]) 
&& !empty($_POST["passwordProf"]) 
&& isset($_POST["submitProf"]) ){
    $req = "INSERT INTO prof(nomProf,prenomProf,sexeProf,adresseProf,telProf,emailProf,password) values(?,?,?,?,?,?,?)";
    $pre = $cnx->prepare($req);
    $pre->execute([
    $_POST["nomProf"],
    $_POST["prenomProf"],
    $_POST['g'],
    $_POST["adresseProf"],
    $_POST["telProf"],
    $_POST["emailProf"],
    $_POST["passwordProf"]
]);
header("location:prof.php?success=vous ajoute avec succéss");
}else{
    if(isset($_POST["submitProf"])){
        header("location:prof.php?err=remplire les champes !!");
    }
}

// rechercher
if(!isset($_POST['submitSerch'])){
    // remplissage prof
    $req3 = "select * from prof";
    $pre3 = $cnx->prepare($req3);
    $pre3->execute();
    $dataProf = $pre3->fetchAll(PDO::FETCH_OBJ);
}
else{
    $serch = $_POST['serch'] ;
    $sql = 'select * from prof where nomProf like ?';
    $pre = $cnx->prepare($sql);
    $pre->execute(["%$serch%"]);
    $dataProf = $pre->fetchAll(PDO::FETCH_OBJ);
}


//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if(isset($_GET['err'])){?>
                    <p class="err"><?=$_GET['err']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['success'])){?>
                    <p class="success"><?=$_GET['success']?></p> ;
         <?php  }?>
         <?php if(isset($_GET['errP'])){?>
                    <p class="err"><?=$_GET['errP']?></p> ;
         <?php  }?>
        <fieldset>
            <legend>Ajouter Professeur</legend>
            <div>
                <input type="text" placeholder="Nom" name="nomProf">
            </div>
            <div>
                <input type="text" placeholder="Prenom" name="prenomProf">
            </div>
            <div>
                <input type="text" placeholder="Adresse" name="adresseProf">
            </div>
            <div>
                <input type="tel" placeholder="Tel" name="telProf">
            </div>
            <div>
                <input type="text" placeholder="Email" name="emailProf">
            </div>
            <div>
                <input type="password" placeholder="Mot de passe" name="passwordProf" />
            </div>
            <div class="radios">
                <input type="radio" name="g" value="Homme">Homme <input type="radio" name="g" value="Femme">Femme 
            </div>

            <div class="btn">
                <input type="submit" value="Ajouter" name="submitProf" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
        <div class="serch">
            <input type="text" placeholder="Rechercher par nom" name="serch" >
            <input type="submit" value="Rechercher" name="submitSerch" >
        </div>
        <div class="affichage">
        <table  class="table">
            <tr>
                <th>ID</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Sexe</th>
                <th>Adresse</th>
                <th>Tel</th>
                <th>Email</th>
                <th>mot de passe</th>
                <th>Supprimer</th>
                <th>Modifier</th>
            </tr>
            <?php
            foreach($dataProf as $dataProf){ ?>
                    <tr>
                        <td><?= $dataProf->idProf ?></td>
                        <td><?= $dataProf->nomProf ?></td>
                        <td><?= $dataProf->prenomProf ?></td>
                        <td><?= $dataProf->sexeProf ?></td>
                        <td><?= $dataProf->adresseProf ?></td>
                        <td><?= $dataProf->telProf ?></td>
                        <td><?= $dataProf->emailProf ?></td>
                        <td><?= $dataProf->password ?></td>
                        <td>
                            <a href='delete.php?idP=<?= $dataProf->idProf ?>'  class='red' >Supprimer</a>
                        </td>
                        <td>
                            <a href='modifierProf.php?id=<?= $dataProf->idProf ?>'  class='green' >Modifier</a>
                        </td>
                    </tr>
            <?php } ?>
        </table>
        </div>
    </form>
</body>

</html>