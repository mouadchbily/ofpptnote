<?php

require_once "cnx.php";

// ===================================================================================
// récupérer les valeurs pésedent
$id = $_GET['id'] ;
$reqet = "SELECT * from Groupe where idGroupe = ? ";
$prepar = $cnx->prepare($reqet);
$prepar->execute([$id]);
$oldData = $prepar->fetch(PDO::FETCH_OBJ);




// modifier Groupe
$err = "" ;
if(!empty($_POST["nomGroupe"]) && isset($_POST["submitGroupe"])){
   try{
        $req = "update Groupe set 
        nomGroupe = ?  where idGroupe = ?";
        $pre = $cnx->prepare($req);
        $pre->execute([$_POST["nomGroupe"],$_GET["id"]]);
        header("location:Groupe.php?success=vous Modifier avec succéss");
   }catch(PDOException){
        $err = "ce groupe existe deje !!";

   }
}else{
    if(isset($_POST["submitGroupe"])){
        $err = "remplire les champes !!";
    }
}

if(isset($_POST['annuler'])){
    header("location:Groupe.php");
}






//=====================================================================================

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="stylle.css">
    <title>Document</title>
    <style>
        fieldset{
            grid-template-columns: repeat(1,1fr);
        }
    </style>
    
</head>
<body>
    <?php include("header.php"); ?>
   
    
    <form method="POST">
        <?php if($err){?>
                    <p class="err"><?=$err?></p>
         <?php  }?>
        <fieldset>
            <legend>Modifier Groupe</legend>
            <div>
                <input type="text" placeholder="Nom" value="<?= $oldData->nomGroupe?>" name="nomGroupe">
            </div>
            <div class="btn">
                <input type="submit" value="Modifier" name="submitGroupe" >
                <input type="reset" value="Annuler">
            </div>

        </fieldset>
    </form>
</body>
</html>